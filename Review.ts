type uuid = string;

export interface Review {
    uuid: uuid; // Идентификатор Review
    pid: uuid | null; // Идентификатор родительского комментария, если таковой существует
    review_type: string;
    star: number;
    pros: {
        text: string;
        format: string;
    };
    cons: {
        text: string;
        format: string;
    };
    body: {
        text: string;
        format: string;
    };
    files: string[];
    video: string[];
    created: number;
    changed: number;
    status: number;
    owner: {
        uuid: uuid;
        login: string;
        firstname: string;
        lastname: string;
        avatar: {
            src: string;
        };
    };
    step: 0;
    thread: string;
}
